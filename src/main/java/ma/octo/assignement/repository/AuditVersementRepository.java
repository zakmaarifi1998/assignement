package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.AuditVersement;

//contient les apis pour les opperations crud
public interface AuditVersementRepository extends JpaRepository<AuditVersement, Long> {

}
