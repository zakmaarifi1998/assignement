package ma.octo.assignement.mapper;
import ma.octo.assignement.domain.Versement;

import ma.octo.assignement.dto.VersementDto;


public class VersementMapper {


    private static VersementDto versementDto; 
    private static Versement versement; 


    public static VersementDto map(Versement versement) {
        versementDto = new VersementDto();
        versementDto.setRibBeneficiaire(versement.getCompteBeneficiaire().getRib());//setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMotif(versement.getMotifVersement());
        versementDto.setMontantVersement(versement.getMontantVersement());

        return versementDto;

    }
    public static Versement mapToEntity(VersementDto versementDto) {
		versement = new Versement();
    	versement.setDateExecution(versementDto.getDate());
    	versement.setMontantVersement(versementDto.getMontantVersement());
    	versement.setMotifVersement(versementDto.getMotif());
		return versement;
	}
}


