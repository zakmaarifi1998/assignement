package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;

@Service
@Transactional
public class VersementServiceImpl implements IVersement{

    public static final int MONTANT_MAXIMAL = 10000;

    @Autowired
    private AuditService monservice;
    @Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VersementRepository versementRepository;
    
	
	@Override
	public List<Versement> loadAll() {
		List<Versement> all = versementRepository.findAll();
		if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
	}

	
	@Override
	public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
	
		Compte compteBeneficiaire = compteRepository.findByNrCompte(versementDto.getRibBeneficiaire());
        
        if (compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        
        if (versementDto.getNomEmetteur().length() <= 0) {
            System.out.println("Nom vide");
            throw new TransactionException("Nom vide");
        }

        if (versementDto.getMontantVersement().equals(null) || versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de versement dépassé");
            throw new TransactionException("Montant maximal de versement dépassé");
        }

        if (versementDto.getMotif().length() <= 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteBeneficiaire);
        
        Versement versement = VersementMapper.mapToEntity(versementDto);
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setNom_prenom_emetteur(versementDto.getNomEmetteur());

        versementRepository.save(versement);

        monservice.auditVirement("Virement depuis " + versementDto.getNomEmetteur() + " vers " + versementDto
                        .getRibBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                        .toString());
		
	}

	@Override
	public void save(Versement versement) {
		versementRepository.save(versement);
		
	}

	
}