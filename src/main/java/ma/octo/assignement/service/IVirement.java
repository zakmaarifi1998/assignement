package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IVirement {
	public List<Virement> loadAll();
	public List<Compte> loadAllCompte();
	public List<Utilisateur> loadAllUtilisateur();
	public void createTransaction(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
	public void save(Virement virement);
}
