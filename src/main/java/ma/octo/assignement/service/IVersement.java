package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IVersement {

	public List<Versement> loadAll();

	public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
	
	public void save(Versement versement);
}

