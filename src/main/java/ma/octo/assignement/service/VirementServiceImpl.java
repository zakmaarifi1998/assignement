package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional

public class VirementServiceImpl implements IVirement{

	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired 
	private VirementRepository virementRepository;
	@Autowired
	private AuditService monservice;
	
    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);
    public static final int MONTANT_MAXIMAL = 10000;
	
	@Override
	public List<Virement> loadAll() {
		List<Virement> all = virementRepository.findAll();
		if(CollectionUtils.isEmpty(all))
			return null;
		return all;
	}

	@Override
	public List<Compte> loadAllCompte() {
		List<Compte> all = compteRepository.findAll();
		if(CollectionUtils.isEmpty(all))
			return null;
		return all;
	}

	@Override
	public List<Utilisateur> loadAllUtilisateur() {
		List<Utilisateur> all = utilisateurRepository.findAll();
		if(CollectionUtils.isEmpty(all))
			return null;
		return all;
	}

	@Override
	public void createTransaction(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

		Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (virementDto.getMontantVirement().equals(null) || virementDto.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        //Le motif ne doit pas etre vide
        if (virementDto.getMotif().length() == 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }
/*repetition de code
        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }*/

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(compteBeneficiaire);

        //la fonction mapToEntity cree un Virement depuis virementDto
        Virement virement = VirementMapper.mapToEntity(virementDto);
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        
        virementRepository.save(virement);

        monservice.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                        .toString());
	}

	@Override
	public void save(Virement virement) {
		virementRepository.save(virement);
		
	}

}