package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersement;
import ma.octo.assignement.service.VersementServiceImpl;

@RestController(value = "/versements")
public class VersementController {

	@Autowired
	private IVersement versementService = new VersementServiceImpl();
	
	@GetMapping("lister_versements")
	List<Versement> loadAll(){
		return versementService.loadAll();
	}
	@PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto) throws  CompteNonExistantException, TransactionException {
    	versementService.createTransaction(versementDto);
        //return ResponseEntity.ok(null);
    }
}
