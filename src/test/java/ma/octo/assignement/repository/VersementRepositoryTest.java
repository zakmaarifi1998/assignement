package ma.octo.assignement.repository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;


@SpringBootTest
@Transactional
public class VersementRepositoryTest {

  @Autowired
  private VersementRepository versementRepository;
  
  @Autowired
  private CompteRepository compteRepository;

  private Versement createVersementTest(Compte compte) {
	  Versement v = new Versement();
		v.setMontantVersement(new BigDecimal("200"));
		v.setCompteBeneficiaire(compte);
		v.setNom_prenom_emetteur("anas");
		v.setDateExecution(new Date());
		v.setMotifVersement("test");
		return v;
  }
  
  @Test
  public void findOne() {
	  
	Versement v = createVersementTest(compteRepository.findAll().get(0));
	versementRepository.save(v);
	Example<Versement> versementExample = Example.of(v);
	Optional<Versement> test = versementRepository.findOne(versementExample);
	assertThat(test.get().getId()).isEqualTo(v.getId());

  }

  @Test
  public void findAll() {
	  Versement v = createVersementTest(compteRepository.findAll().get(0));
	  versementRepository.save(v);
	  List<Versement> list = versementRepository.findAll();
	  assertThat(list.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
	  	
	  	Versement v = createVersementTest(compteRepository.findAll().get(0));
		versementRepository.save(v);
		assertThat(versementRepository.findAll().contains(v));
  }

  @Test
  public void delete() {
	  	Versement v = createVersementTest(compteRepository.findAll().get(0));
		versementRepository.save(v);
		versementRepository.delete(v);
		assertThat(versementRepository.findAll()).doesNotContain(v);
  }
}
