package ma.octo.assignement.repository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;
  
  @Autowired
  private CompteRepository compteRepository;

  @Test
  public void findOne() {
	  List<Virement> list = virementRepository.findAll();
	  Virement virement = virementRepository.findById(list.get(0).getId()).get();
	  //Optional<Virement> test = virementRepository.findOne((Example<Virement>) virement);
	  Example<Virement> virementExample = Example.of(virement);
	  Optional<Virement> test = virementRepository.findOne(virementExample);
	  assertThat(test.get().getId()).isEqualTo(virement.getId());

  }

  @Test
  public void findAll() {
	  List<Virement> list = virementRepository.findAll();
	  assertThat(list.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
	  	
	  	List<Compte> listCompte= compteRepository.findAll();
	  	Virement v = new Virement();
		v.setMontantVirement(new BigDecimal("200"));
		v.setCompteBeneficiaire(listCompte.get(0));
		v.setCompteEmetteur(listCompte.get(1));
		v.setDateExecution(new Date());
		v.setMotifVirement("test");

		virementRepository.save(v);
		assertThat(virementRepository.findAll().contains(v));
  }

  @Test
  public void delete() {
	  Virement v = virementRepository.findAll().get(0);
	  virementRepository.delete(v);
	  assertThat(virementRepository.findAll()).doesNotContain(v);
  }
}
