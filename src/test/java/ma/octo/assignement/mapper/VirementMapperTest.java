package ma.octo.assignement.mapper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;

@SpringBootTest
@Transactional
class VirementMapperTest {

	@Autowired
	private CompteRepository compteRepository;
	
	@Test
	void map() {
        
        Virement virement = new Virement();
        virement.setId((long) 10);
        virement.setCompteEmetteur(compteRepository.findAll().get(0));
        virement.setCompteBeneficiaire(compteRepository.findAll().get(1));
        virement.setMontantVirement(new BigDecimal("100"));
        virement.setMotifVirement("test");
        virement.setDateExecution(new Date());
        
        VirementDto virementDto = VirementMapper.map(virement);
        
        assertThat(virementDto.getNrCompteEmetteur()).isEqualTo(virement.getCompteEmetteur().getNrCompte());
        assertThat(virementDto.getMontantVirement()).isEqualTo(virement.getMontantVirement());
        assertThat(virementDto.getMotif()).isEqualTo(virement.getMotifVirement());
        assertThat(virementDto.getNrCompteBeneficiaire()).isEqualTo(virement.getCompteBeneficiaire().getNrCompte());
        assertThat(virementDto.getDate()).isEqualTo(virement.getDateExecution());



	}

}

