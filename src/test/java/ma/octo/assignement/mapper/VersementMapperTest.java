package ma.octo.assignement.mapper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;

@SpringBootTest
@Transactional
class VersementMapperTest {

	@Autowired
	private CompteRepository compteRepository;
	
	@Test
	void map() {
        
        Versement versement = new Versement();
        versement.setId((long) 10);
        versement.setNom_prenom_emetteur("zakaria");
        versement.setCompteBeneficiaire(compteRepository.findAll().get(0));
        versement.setMontantVersement(new BigDecimal("100"));
        versement.setMotifVersement("test");
        versement.setDateExecution(new Date());
        
        VersementDto versementDto = VersementMapper.map(versement);
        
       /* assertThat(versementDto.getNomEmetteur()).isEqualTo(versement.getNom_prenom_emetteur());*/
        assertThat(versementDto.getMontantVersement()).isEqualTo(versement.getMontantVersement());
        assertThat(versementDto.getMotif()).isEqualTo(versement.getMotifVersement());
        assertThat(versementDto.getRibBeneficiaire()).isEqualTo(versement.getCompteBeneficiaire().getRib());
        assertThat(versementDto.getDate()).isEqualTo(versement.getDateExecution());



	}

}

